p2p-webrtc-screen-sharing: peer-to-peer WebRTC Screen Sharing
=============================================================

Based on: https://www.webrtc-experiment.com/Pluginfree-Screen-Sharing/

Changes:
- upgrade to socket.io 2.x
- runs signalling server in a subdir

References:
- https://github.com/muaz-khan/WebRTC-Experiment/tree/master/socketio-over-nodejs
- https://github.com/muaz-khan/WebRTC-Experiment/tree/master/Pluginfree-Screen-Sharing
- https://github.com/muaz-khan/WebRTC-Experiment/issues/649
- https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
- https://socket.io/docs/client-api/#With-websocket-transport-only
- https://www.nginx.com/blog/websocket-nginx/

# Requirements

Tested on Debian 10 (buster):

    sudo apt install yarnpkg nginx make

Install JavaScript dependencies with:

    make

# Set-up

Configure nginx to serve the `www` directory over https.

Start server.js over http:

    nodejs server.js

modify `www/index.html` to point to your `SIGNALING_SERVER`.

Configure nginx proxy for the signalling server:

    location /webrtc/ {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Uri $uri;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header Host $host;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "Upgrade";
      proxy_pass http://x.y.w.z:9559;
      proxy_redirect default;
    }
